package view.utils;

public enum ControllerVariablesNames {

    /**
     *
     */
    NORTH,

    /**
     *
     */
    SOUTH,

    /**
     *
     */
    EAST,

    /**
     *
     */
    WEST,

    /**
     *
     */
    MAXVEL,

    /**
     *
     */
    GREENLIGHT,


    /**
     *
     */
    NLANES;
}
