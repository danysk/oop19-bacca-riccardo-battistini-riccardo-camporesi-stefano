package constraints;

public enum CellConstraints {

    /**
     * 
     */
    SIMPLE,

    /**
     * 
     */
    START,

    /**
     * 
     */
    END,

    /**
     * 
     */
    CROSSROAD
}
