package model;

public enum ScenarioStatus {

    /**
     * 
     */
    RUNNING,

    /**
     * 
     */
    STOPPED,

    /**
     * 
     */
    SETUP;
}
